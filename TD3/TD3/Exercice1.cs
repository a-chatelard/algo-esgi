﻿using System;

namespace TD3
{
    public static class Exercice1
    {
        public static void Main1()
        {
            Console.WriteLine("4°)");
            Console.WriteLine("Création de l'arbre...");
            var node20 = new Noeud { Etiquette = 20 };
            var node5 = new Noeud { Etiquette = 5 };
            var node25 = new Noeud { Etiquette = 25 };
            var node21 = new Noeud { Etiquette = 21 };
            var node28 = new Noeud { Etiquette = 28 };
            var node3 = new Noeud { Etiquette = 3 };
            var node12 = new Noeud { Etiquette = 12 };
            var node8 = new Noeud { Etiquette = 8 };
            var node13 = new Noeud { Etiquette = 13 };
            var node6 = new Noeud { Etiquette = 6 };

            node20.Enfants[0] = node5;
            node20.Enfants[1] = node25;
            node5.Enfants[0] = node3;
            node5.Enfants[1] = node12;
            node25.Enfants[0] = node21;
            node25.Enfants[1] = node28;
            node12.Enfants[0] = node8;
            node12.Enfants[1] = node13;
            node8.Enfants[0] = node6;

            Console.WriteLine("Lecture infixe de l'arbre : ");

            node20.ReadChild();
        }
    }
}
