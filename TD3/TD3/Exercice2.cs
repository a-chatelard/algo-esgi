﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TD3
{
    public static class Exercice2
    {
        public static void Main2()
        {
            Console.WriteLine("2°)");
            Console.WriteLine("Génération des noeuds...");
            var i = 10000;
            var nodeList = ListTool.GenerateNodeList(i);
            Console.WriteLine("Mélange...");
            nodeList.Shuffle();
            Console.WriteLine($"Création de l'arbre binaire de recherche, nombre d'éléments {i} ...");
            ListTool.BuildBinarySearchTree(nodeList);
            Console.WriteLine("3°)");
            Console.WriteLine("Création d'une liste de 100 nombres au hasard...");
            var numberList = ListTool.GetRandomNumbers(100, i);
            Console.WriteLine("Recherche via arbre binaire de recherche...");
            FindManyInList(nodeList, numberList);
            Console.WriteLine("Recherche via fonction find comprise dans net...");
            FindManyInListWithNetFind(nodeList, numberList);
            Console.WriteLine("4°)");
            Console.WriteLine("Génération des noeuds...");
            nodeList = ListTool.GenerateNodeList(i);
            Console.WriteLine("Mélange...");
            nodeList.Shuffle();
            Console.WriteLine("Tri via arbre binaire de recherche...");
            SortList(nodeList);
            Console.WriteLine("Tri via fonction Sort() comprise dans net...");
            SortNet(nodeList);
        }

        private static void FindManyInList(List<Noeud> noeuds, List<int> nombres)
        {
            var dateDebut = DateTime.Now;
            foreach (var nombre in nombres)
            {
                noeuds[0].FindInBinarySearchTree(nombre);
            }
            var dateFin = DateTime.Now;
            Console.WriteLine("Temps passé pour trouver tous les nombres via arbre binaire de recherche : " + (dateFin - dateDebut));
        }

        private static void FindManyInListWithNetFind(List<Noeud> noeuds, List<int> nombres)
        {
            var dateDebut = DateTime.Now;
            foreach (var nombre in nombres)
            {
                noeuds.Find(n => n.Etiquette == nombre);
            }
            var dateFin = DateTime.Now;
            Console.WriteLine("Temps passé pour trouver tous les nombres via fonction existante : " + (dateFin - dateDebut));
        }

        private static void SortList(List<Noeud> nodeList)
        {
            var dateDebut = DateTime.Now;
            ListTool.BuildBinarySearchTree(nodeList);
            var dateFin = DateTime.Now;
            Console.WriteLine("Temps passé pour trier une liste en utilisant un arbre binaire de recherche : " + (dateFin - dateDebut));
        }

        private static void SortNet(List<Noeud> nodeList)
        {
            var dateDebut = DateTime.Now;
            nodeList.Sort();
            var dateFin = DateTime.Now;
            Console.WriteLine("Temps passé pour trier une liste en utilisant la fonction .net Sort() : " + (dateFin - dateDebut));
        }
    }
}
