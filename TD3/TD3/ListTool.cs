﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TD3
{
    public static class ListTool
    {
        private static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static List<Noeud> GenerateNodeList(int iteration)
        {
            var list = new List<Noeud>();
            for (int i = 1; i <= iteration; i++)
            {
                list.Add(new Noeud { Etiquette = i });
            }
            return list;
        }

        public static void BuildBinarySearchTree(List<Noeud> noeuds)
        {
            var dateDebut = DateTime.Now;
            var racine = noeuds[0];
            foreach (var node in noeuds.Skip(1))
            {
                racine.SetChild(node);
            }
            var dateFin = DateTime.Now;
            Console.WriteLine("Temps passé pour construire l'arbre binaire : " + (dateFin - dateDebut));
        }

        public static List<int> GetRandomNumbers(int howMany, int max)
        {
            var list = new List<int>();
            for(int i = 0; i < howMany; i++)
            {
                var rdn = rng.Next(max);
                list.Add(rdn);
            }
            return list;
        }
    }
}
