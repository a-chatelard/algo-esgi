﻿using System;

namespace TD3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Exercice 1 : ");
            Exercice1.Main1();

            Console.WriteLine("\n\nExercice 2 : ");
            Exercice2.Main2();
        }
    }
}
