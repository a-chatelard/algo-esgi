﻿using System;
using System.Collections.Generic;

namespace TD3
{
    public class Noeud : IComparable
    {
        public int Etiquette { get; set; }
        public List<Noeud> Enfants { get; set; } = new List<Noeud>() { null, null };


        public void SetChild(Noeud node)
        {
            if (node.Etiquette < Etiquette)
            {
                if (Enfants[0] == null)
                {
                    Enfants[0] = node;
                } else
                {
                    Enfants[0].SetChild(node);
                }
            } 
            else if (node.Etiquette > Etiquette)
            {
                if (Enfants[1] == null)
                {
                    Enfants[1] = node;
                } else
                {
                    Enfants[1].SetChild(node);
                }
            }
        }

        public void ReadChild()
        {
            if (Enfants.Count > 2)
            {
                throw new Exception("L'arbre n'est pas logique.");
            }
            if (Enfants.Count > 0 && Enfants[0] != null)
            {
                Enfants[0].ReadChild();
            }
            Console.Write(Etiquette + " ");
            if (Enfants.Count > 1 && Enfants[1] != null)
            {
                Enfants[1].ReadChild();
            }
        }

        public Noeud FindInBinarySearchTree(int intToFind)
        {
            if (Etiquette > intToFind)
            {
                return Enfants[0].FindInBinarySearchTree(intToFind);
            } else if (Etiquette < intToFind)
            {
                return Enfants[1].FindInBinarySearchTree(intToFind);
            }
            return this;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Noeud otherNode = obj as Noeud;
            return Etiquette.CompareTo(otherNode.Etiquette);
        }
    }
}
