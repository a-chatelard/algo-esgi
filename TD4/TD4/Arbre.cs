﻿using System;
using System.Collections.Generic;

namespace TD4
{
    public class Arbre
    {
        public Noeud Racine { get; set; }

        public bool IsEquilibre()
        {
            var hauteurGauche = Racine.Enfants[0].ParcourirEnProfondeurSuffixe(1, new List<int>());

            var hauteurDroite = Racine.Enfants[1].ParcourirEnProfondeurSuffixe(1, new List<int>());

            var facteur = Math.Abs(hauteurDroite - hauteurGauche);

            return facteur <= 1;
        }

        public void RotationGauche(Noeud noeud)
        {
            if (noeud.Enfants[1].Enfants[0] == null && noeud.Enfants[0] == null)
            {
                noeud.Enfants[0] = noeud.Enfants[1].Enfants[1];
                noeud.Enfants[1].Enfants[1] = null;

            } else
            {
                Console.WriteLine("Rotation impossible");
            }
        }



        public static Arbre BuildArbre()
        {
            var arbre = new Arbre();

            var n20 = new Noeud();
            var n25 = new Noeud();
            var n21 = new Noeud();
            var n28 = new Noeud();
            var n5 = new Noeud();
            var n3 = new Noeud();
            var n12 = new Noeud();
            var n8 = new Noeud();
            var n13 = new Noeud();
            var n6 = new Noeud();

            n20.Enfants[0] = n5;
            n20.Enfants[1] = n25;
            n5.Enfants[0] = n3;
            n5.Enfants[1] = n12;
            n12.Enfants[0] = n8;
            n12.Enfants[1] = n13;
            n8.Enfants[0] = n6;
            n25.Enfants[0] = n21;
            n25.Enfants[1] = n28;

            arbre.Racine = n20;

            return arbre;
        }
    }
}
