﻿using System.Collections.Generic;
using System.Linq;

namespace TD4
{
    public class Noeud
    {
        public Noeud() { }

        public Noeud(string label)
        {
            Label = label;
        }

        public string Label = null;
        public List<Noeud> Enfants { get; set; } = new List<Noeud>() { null, null };

        public int ParcourirEnProfondeurSuffixe(int compteur, List<int> longueurBranches)
        {
            if (Enfants[0] != null || Enfants[1] != null)
            {
                compteur++;
                foreach(var enfant in Enfants)
                {
                    if (enfant != null)
                    enfant.ParcourirEnProfondeurSuffixe(compteur, longueurBranches);
                }
            }
            longueurBranches.Add(compteur);
            compteur = 0;
            return longueurBranches.Max();
        }
    }
}
