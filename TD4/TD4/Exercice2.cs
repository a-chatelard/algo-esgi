﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD4
{
    public static class Exercice2
    {
        public static void Main1()
        {
            #region Construction arbre exercice 2
            var arbre = new Arbre();

            var n10 = new Noeud();
            var n12 = new Noeud("12");
            var n15 = new Noeud("15");
            var n17 = new Noeud("17");
            var n5 = new Noeud();
            var n7 = new Noeud();
            var n2 = new Noeud();
            var n4 = new Noeud();

            n10.Enfants[0] = n5;
            n10.Enfants[1] = n12;
            n5.Enfants[0] = n2;
            n5.Enfants[1] = n7;
            n2.Enfants[0] = n4;
            n12.Enfants[1] = n15;
            n15.Enfants[1] = n17;

            arbre.Racine = n10;

            #endregion
            Console.WriteLine("Enfants actuels du noeux 12 :");
            foreach (var enfant in n12.Enfants)
            {
                if (enfant != null)
                {
                    Console.Write(enfant.Label + " ");
                }
            }
            Console.WriteLine("\nRotation gauche autour du noeud");
            arbre.RotationGauche(n12);
            Console.WriteLine("Nouveaux enfants du noeux 12 :");
            foreach (var enfant in n12.Enfants)
            {
                Console.Write(enfant.Label + " ");
            }
        }
    }
}
