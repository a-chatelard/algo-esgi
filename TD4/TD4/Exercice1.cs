﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD4
{
    public static class Exercice1
    {
        public static void Main1()
        {
            Console.WriteLine("3°)");
            var arbre = Arbre.BuildArbre();
            var hauteurGauche = arbre.Racine.Enfants[0].ParcourirEnProfondeurSuffixe(1, new List<int>());
            Console.WriteLine("La hauteur de l'arbre côté gauche est " + hauteurGauche);

            var hauteurDroite = arbre.Racine.Enfants[1].ParcourirEnProfondeurSuffixe(1, new List<int>());
            Console.WriteLine("La hauteur de l'arbre côté droit est " + hauteurDroite);

            Console.WriteLine("\n4°)");
            if (arbre.IsEquilibre())
            {
                Console.WriteLine("L'arbre est équilibré.");
            } else
            {
                Console.WriteLine("L'arbre n'est pas équilibré.");
            }
        }
    }
}
