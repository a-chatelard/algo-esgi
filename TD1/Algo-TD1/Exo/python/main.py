from keras.layers import Dense
from keras.models import Sequential


def linear_model_keras():
    model = Sequential()
    model.add(Dense(1, input_shape(9, 1)))
    return model


model = linear_model_keras()
model.compile(optimizer="SGD", loss="mean_squared_error")
