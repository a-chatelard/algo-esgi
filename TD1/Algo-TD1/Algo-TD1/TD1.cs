﻿using System;
using System.Collections.Generic;

namespace Algo_TD1
{
    public class TD1
    {
        public void RunExercices()
        {
            // Exercice 1, 2
            Exercice1();
        }

        private void Exercice1()
        {
            #region declaration arbres
            var ta = new Noeux("a");
            var tb = new Noeux("b");
            var tc = new Noeux("c");
            var td = new Noeux("d");
            var te = new Noeux("e");
            var tf = new Noeux("f");
            var tg = new Noeux("g");
            var th = new Noeux("h");
            var ti = new Noeux("i");
            var tj = new Noeux("j");
            var tk = new Noeux("k");
            var tl = new Noeux("l");
            var tm = new Noeux("m");
            var tn = new Noeux("n");
            var to = new Noeux("o");
            var tp = new Noeux("p");

            ta.Enfants = new List<Noeux>() { tb, tc, td };
            tb.Enfants = new List<Noeux>() { te, tf };
            td.Enfants = new List<Noeux>() { tg, th, ti };
            te.Enfants = new List<Noeux>() { tj, tk, tl };
            tg.Enfants.Add(tm);
            ti.Enfants = new List<Noeux>() { tn, to };
            tm.Enfants.Add(tp);
            #endregion
            Console.WriteLine("\nExercice 2\n");
            Exercice2(ta);
        }

        private void Exercice2(Noeux t)
        {
            Console.WriteLine("Partie 1\n");
            #region  partie 1
            Console.WriteLine("Parcours en largeur");
            var resultat = new List<Noeux>();
            ParcourirEnLargeur(t, resultat);

            foreach (var arbre in resultat)
            {
                Console.Write(arbre.Id + " ");
            }

            Console.WriteLine("\nParcours en profondeur préfixe");
            resultat = new List<Noeux>();
            ParcourirEnProfondeurPrefixe(t, resultat);

            foreach (var arbre in resultat)
            {
                Console.Write(arbre.Id + " ");
            }

            Console.WriteLine("\nParcours en profondeur suffixe");
            resultat = new List<Noeux>();
            ParcourirEnProfondeurSuffixe(t, resultat);

            foreach (var arbre in resultat)
            {
                Console.Write(arbre.Id + " ");
            }
            #endregion

            Console.WriteLine("\n\nPartie 3");
            #region partie 3
            var t20 = new Noeux("20");
            var t5 = new Noeux("5");
            var t25 = new Noeux("25");
            var t3 = new Noeux("3");
            var t12 = new Noeux("12");
            var t21 = new Noeux("21");
            var t28 = new Noeux("28");
            var t8 = new Noeux("8");
            var t13 = new Noeux("13");
            var t6 = new Noeux("6");

            t20.Enfants = new List<Noeux>() { t5, t25 };
            t5.Enfants = new List<Noeux>() { t3, t12 };
            t25.Enfants = new List<Noeux>() { t21, t28 };
            t12.Enfants = new List<Noeux>() { t8, t13 };
            t8.Enfants.Add(t6);

            Console.WriteLine("\nParcours en largeur");
            resultat = new List<Noeux>();
            ParcourirEnLargeur(t20, resultat) ;

            foreach (var arbre in resultat)
            {
                Console.Write(arbre.Id + " ");
            }

            Console.WriteLine("\nParcours en profondeur préfixe");
            resultat = new List<Noeux>();
            ParcourirEnProfondeurPrefixe(t20, resultat);

            foreach (var arbre in resultat)
            {
                Console.Write(arbre.Id + " ");
            }

            Console.WriteLine("\nParcours en profondeur suffixe");
            resultat = new List<Noeux>();
            ParcourirEnProfondeurSuffixe(t20, resultat);

            foreach (var arbre in resultat)
            {
                Console.Write(arbre.Id + " ");
            }

            Console.WriteLine("\nParcours en profondeur infixe");
            resultat = new List<Noeux>();
            ParcourirEnProfondeurInfixe(t20, resultat);

            foreach (var arbre in resultat)
            {
                Console.Write(arbre.Id + " ");
            }
            #endregion
        }

        #region parcours arbre
        private void ParcourirEnLargeur(Noeux t, List<Noeux> resultat)
        {
            var noeudsActuel = new Queue<Noeux>();
            noeudsActuel.Enqueue(t);
            while (noeudsActuel.Count != 0)
            {
                Noeux noeudActuel = noeudsActuel.Dequeue();
                resultat.Add(noeudActuel);
                foreach (var enfant in noeudActuel.Enfants)
                {
                    noeudsActuel.Enqueue(enfant);
                }
            }
        }

        private void ParcourirEnProfondeurPrefixe(Noeux t, List<Noeux> resultat)
        {
            resultat.Add(t);
            foreach (var enfant in t.Enfants)
            {
                ParcourirEnProfondeurPrefixe(enfant, resultat);
            }
        }

        public void ParcourirEnProfondeurSuffixe(Noeux t, List<Noeux> resultat)
        {
            foreach (var enfant in t.Enfants)
            {
                ParcourirEnProfondeurSuffixe(enfant, resultat);
            }
            resultat.Add(t);
        }

        private void ParcourirEnProfondeurInfixe(Noeux t, List<Noeux> resultat)
        {
            if (t.Enfants.Count > 2)
            {
                throw new Exception("L'arbre n'est pas logique.");
            }
            if (t.Enfants.Count > 0)
            {
                ParcourirEnProfondeurInfixe(t.Enfants[0], resultat);
            }
            resultat.Add(t);
            if (t.Enfants.Count > 1)
            {
                ParcourirEnProfondeurInfixe(t.Enfants[1], resultat);
            }
        }
        #endregion
    }
}
