﻿using System.Collections.Generic;

namespace Algo_TD1
{
    public class Noeux
    {
        public string Id { get; set; }
        public List<Noeux> Enfants { get; set; } = new List<Noeux>();

        public Noeux(string id)
        {
            Id = id;
        }
    }
}
